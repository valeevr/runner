﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

public class PlayerMovement : MonoBehaviour
{
    public GameManager gameManager;
    GameObject ball;
    CharacterController cc;
    Animator animator;
    public AnimationClip clip;
    public Text speedText;
    Vector3 moveVec;
    Vector3 position;
    public Score sc;
    float speed = 5;
    float initialspeed;
    int died = 0;
    private float startTime;
    private float verticalVelocity = 0.0f;
    private float gravity = 12;
    private float animationDuration = 2.0f;
    public bool isDead = false;
    private bool pressed = false;
    public bool hitEnemy = false;
    int life = 3;
    bool slowed;
    bool shrink;
    public bool nextlevel;
    bool speedup;
    void Start()
    {

        cc = GetComponent<CharacterController>();
        startTime = Time.time;
        animator = GetComponent<Animator>();
        initialspeed = speed;
        ball = gameManager.ball;
        speedText.text = "Speed: " + speed.ToString("F1");
        animator.SetBool("run", true);


    }

    void FixedUpdate()
    {

        if (isDead == true)
            return;

        if (cc.isGrounded)
        {
            verticalVelocity = -0.5f;
        }
        else
        {
            verticalVelocity -= gravity * Time.deltaTime;
        }
        moveVec = Vector3.zero;
        moveVec.x = 0;
        moveVec.y = verticalVelocity;
        moveVec.z = speed;
        cc.Move(moveVec * Time.deltaTime);

        if (transform.position.y < -3.5)
        {
            PlayerDeath();
        }
        if (transform.position.x != 0)
        {
            transform.position = new Vector3(0, transform.position.y, transform.position.z);
        }

    }

    public void IncreaseSpeed(float mod)
    {
        speed = initialspeed;
        speed = speed + mod;
        speedText.text = "Speed: " + speed.ToString("F1");
        initialspeed = speed;
    }



    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        var tag = hit.gameObject.tag;


        if (tag == "Enemy")
        {
            if (hitEnemy == false)
            {
                hitEnemy = true;
                PlayerDeath();
                ReportEnd(gameManager.level, "enemy");
            }
        }


        if (tag == "Slow")
        {
            if (slowed == false)
            {
                StartCoroutine(DecreaseSpeed(1));
                slowed = true;
            }
        }
        if (tag == "Catch")
        {
            if (nextlevel == false)
            {
                gameManager.soundManager.CatchSound();
                nextlevel = true;
                gameManager.NextLevel();

            }
        }
        if (tag == "Shrink")
        {
            Destroy(hit.gameObject);
            gameManager.spawner.GetComponent<Spawner>().activeGO.Remove(hit.gameObject);
            if (shrink == false)
            {
                shrink = true;
                StartCoroutine(ShrinkSize());

            }
        }
        if (tag == "Speed")
        {
            Destroy(hit.gameObject);
            gameManager.spawner.GetComponent<Spawner>().activeGO.Remove(hit.gameObject);
            
            StartCoroutine(SetSpeed(1));
            
        }
    }

    public void PlayerDeath()
    {
        gameManager.soundManager.HitEnemy();
        isDead = true;
        sc.GetComponent<Score>().onDeath();
        animator.SetBool("Die", true);
        gameManager.deaths += 1;
        animator.SetBool("run", false);
        gameManager.ball.GetComponent<CatchMe>().paused = true;
        Debug.Log(gameManager.deaths);
        PlayerPrefs.SetInt("deaths", gameManager.deaths);
        AddToScore(GPGSIds.leaderboard_highscore, (long)gameManager.score.GetComponent<Score>().score);
        if (gameManager.deaths >= 2)
        {
            gameManager.unityAds.ShowAd();
            gameManager.deaths = 0;
            PlayerPrefs.DeleteKey("deaths");
        }
    }

    public void ResetPlayer()
    {
        speed = 5;
        initialspeed = speed;
        animator.SetBool("run", true);
        isDead = false;
        hitEnemy = false;
        slowed = false;
        nextlevel = false;
        speedup = false;
        speedText.text = "Speed: " + speed.ToString("F1");
        transform.position = new Vector3(transform.position.x, -2.395f, transform.position.z);

    }
    public IEnumerator Aim()
    {

        GetComponent<Animator>().SetBool("aim", true);
        yield return new WaitForSeconds(0.25f);
        GetComponent<Animator>().SetBool("aim", false);

    }

    public IEnumerator DecreaseSpeed(float mod)
    {
        gameManager.soundManager.SlowDown();
        speed -= mod;
        speedText.text = "Speed: " + speed.ToString("F1");
        yield return new WaitForSeconds(5.0f);
        speed = initialspeed;
        slowed = false;
        speedText.text = "Speed: " + speed.ToString("F1");
        gameManager.soundManager.hit_slow = false;
    }

    public IEnumerator SetSpeed(float modifier)
    {
        gameManager.soundManager.SpeedUp();
        speed = speed + modifier;
        speedText.text = "Speed: " + speed.ToString("F1");
        yield return new WaitForSeconds(2.0f);
        speed = initialspeed;
        speedText.text = "Speed: " + speed.ToString("F1");
        gameManager.soundManager.hit_speedup = false;
    }

    public IEnumerator ShrinkSize()
    {
        gameManager.soundManager.Shrink();
        transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        yield return new WaitForSeconds(4.0f);
        transform.localScale = new Vector3(2, 2, 2);
        shrink = false;
        gameManager.soundManager.hit_shrink = false;
    }

    public void ReportEnd(int level, string reason)
    {
        AnalyticsEvent.Custom("level failed", new Dictionary<string, object>
    {
        { "level", level},
        { "reason", reason}
    });
    }

   

    public static void AddToScore(string leaderboardID, long score)
    {
        Social.ReportScore(score, leaderboardID, success => { });
    }
}

