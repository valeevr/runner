﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUp : MonoBehaviour {
    public PlayerMovement player;
    public Spawner spawner;
    // Use this for initialization
    void Start () {
        gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void DecreaseSpeed()
    {
        player.GetComponent<PlayerMovement>().SetSpeed(-0.5f);
        gameObject.SetActive(false);
    }
    public void IncreaseRange()
    {
        spawner.IncreaseDistance(2);
        gameObject.SetActive(false);
    }
}
