﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class GameManager : MonoBehaviour
{
    public GameObject player;
    public GameObject blocksPool;
    public GameObject swipeManager;
    public GameObject spawner;
    public GameObject score;
    public GameObject ball;
    public SoundManager soundManager;
    
    public UnityAds unityAds;
    public TimerManager timer;
    public TileManager tileManager;
    public Material walls;
    public Material ground;
    public int level = 1;
    public int deaths=0;
    float speedIncrease = 0.1f;
    int switcher;
    public Text levelText;
    public Text nextCube;
    float initialDistance;
    float initialTimer;
    float distanceMod;
    Vector3 distanceCube;
    public Color[] colorlist;
    public Color initialColor;
    // Use this for initialization
    void Start()
    {
        initialTimer = timer.timer;
        initialDistance = ball.transform.position.z - player.transform.position.z;
        distanceMod = initialDistance;
        levelText.text = "Level: " + level.ToString();

        walls.color = initialColor;
        ground.color = initialColor;
        switcher = 0;
        if(PlayerPrefs.HasKey("deaths"))
        {
            deaths = PlayerPrefs.GetInt("deaths");   
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(spawner.GetComponent<Spawner>().activeGO.Count>0)
        { 
            distanceCube = player.transform.position - spawner.GetComponent<Spawner>().activeGO[0].transform.position;
            if (distanceCube.z > 0.5f)
            { 
                Destroy(spawner.GetComponent<Spawner>().activeGO[0]);
                spawner.GetComponent<Spawner>().activeGO.RemoveAt(0);
            }
        }

    }

    public void NextLevel()
    {
        level = level + 1;
        switcher += 1;
        if (switcher > 4)
            switcher = 1;

        switch (level)
        {
            case 2:
                nextCube.text = "Next: Speed Decrease";
                spawner.GetComponent<Spawner>().obstacles.Add(spawner.GetComponent<Spawner>().speedCube);
                spawner.GetComponent<Spawner>().obstacles.Add(spawner.GetComponent<Spawner>().speedCube);
                break;
            case 3:
                spawner.GetComponent<Spawner>().obstacles.Add(spawner.GetComponent<Spawner>().slowCube);
                nextCube.text = "Next: Instant Death";
            break;
            case 4:
                spawner.GetComponent<Spawner>().obstacles.Add(spawner.GetComponent<Spawner>().deathCube);
                nextCube.text = "Next: Shrink size";
            break;
            case 5:
                spawner.GetComponent<Spawner>().obstacles.Add(spawner.GetComponent<Spawner>().shrinkCube);
                spawner.GetComponent<Spawner>().obstacles.Add(spawner.GetComponent<Spawner>().TutorialCube);
                nextCube.text = "Next: More Speed";
            break;
            case 6:
                spawner.GetComponent<Spawner>().obstacles.Add(spawner.GetComponent<Spawner>().speedCube);
                spawner.GetComponent<Spawner>().obstacles.Add(spawner.GetComponent<Spawner>().speedCube);
                spawner.GetComponent<Spawner>().obstacles.Add(spawner.GetComponent<Spawner>().TutorialCube);
                nextCube.text = "Next: More Death";
                break;
            case 7:
                spawner.GetComponent<Spawner>().obstacles.Add(spawner.GetComponent<Spawner>().deathCube);
                spawner.GetComponent<Spawner>().obstacles.Add(spawner.GetComponent<Spawner>().deathCube);
                nextCube.text = "Next Cube: More Shrink";
                break;
            case 8:
                spawner.GetComponent<Spawner>().obstacles.Add(spawner.GetComponent<Spawner>().shrinkCube);
                spawner.GetComponent<Spawner>().obstacles.Add(spawner.GetComponent<Spawner>().shrinkCube);
                nextCube.text = "Next Cube: Random";
                break;

        }
        levelText.text = "Level: " + level.ToString();
        distanceMod += level;
        timer.timer = initialTimer + distanceMod;
        ball.transform.position = player.transform.position + new Vector3(0,0,distanceMod);
        ball.GetComponent<CatchMe>().SetSpeed(speedIncrease);
        player.GetComponent<PlayerMovement>().IncreaseSpeed(speedIncrease);
        player.GetComponent<PlayerMovement>().nextlevel = false;
        walls.color = colorlist[switcher];
        ground.color = colorlist[switcher];
        ReportSwitch(level);
        soundManager.ResetHits();

    }

    public void ResetLevel()
    {
        level = 1;
        levelText.text = "Level: " + level.ToString();
        int distanceMod = 10;
        nextCube.text = "Next Cube: Slow";
       
        timer.Reset();
       
        player.GetComponent<PlayerMovement>().ResetPlayer();
        ball.GetComponent<CatchMe>().ResetBall();
        ball.transform.position = player.transform.position + new Vector3(0, 0, distanceMod);
        player.transform.position=ball.transform.position-new Vector3(0, 0, 5);
        ball.transform.position = player.transform.position + new Vector3(0, 0, distanceMod);
        score.GetComponent<Score>().ResetScore();
        spawner.GetComponent<Spawner>().ResetSpawn();
        initialTimer = timer.timer;
        walls.color = initialColor;
        ground.color = initialColor;
        switcher = level;
       
        for (int i = 0; i < blocksPool.transform.childCount; i++)
        {
            Destroy(blocksPool.transform.GetChild(i).gameObject);
        }


    }

    public void ReportSwitch(int level)
    {
        AnalyticsEvent.Custom("level switched", new Dictionary<string, object>
    {
        { "level", level}
    });
    }
}
