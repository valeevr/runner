﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;

public class UnityAds : MonoBehaviour
{

    public string placementId = "video";
    private string gameId = "2983385";
    bool testMode = true;
    public GameManager gameManager;
    void Start()
    {
            Advertisement.Initialize(gameId, testMode);
    }

    public void ShowAd()
    {
        StartCoroutine(ShowAdWhenReady());
    }

    private IEnumerator ShowAdWhenReady()
    {
        while (!Advertisement.IsReady(placementId))
        {
            yield return new WaitForSeconds(0.25f);
        }
        var options = new ShowOptions { resultCallback = HandleShowResult };
        Advertisement.Show(placementId, options);
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                gameManager.deaths = 0;
                PlayerPrefs.DeleteKey("deaths");
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                break;
            case ShowResult.Skipped:
                gameManager.deaths = 0;
                PlayerPrefs.DeleteKey("deaths");
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                gameManager.deaths = 0;
                PlayerPrefs.DeleteKey("deaths");
                Debug.LogError("The ad failed to be shown.");
                break;
        }

    }
}