﻿using UnityEngine;
using System.Collections;

public class CatchMe : MonoBehaviour
{
    CharacterController cc;
    bool catched=false;
    private float verticalVelocity = 0.0f;
    private float gravity = 12;
    Vector3 moveVec;
    float speed = 4.0f;
    public int roll;
    public bool paused;
    // Use this for initialization
    void Start()
    {
        cc = GetComponent<CharacterController>();
        roll = 25;
    }

    // Update is called once per frame
    void Update()
    {
        if (paused == true)
            return;

        if (catched == true)
            return;

        if (cc.isGrounded)
        {
            verticalVelocity = -0.5f;
        }
        else
        {
            verticalVelocity -= gravity * Time.deltaTime;
        }
        moveVec = Vector3.zero;
        moveVec.x = 0;
        moveVec.y = verticalVelocity;
        moveVec.z = speed;
        cc.Move(moveVec * Time.deltaTime);
        transform.Rotate(roll, 0, 0);
    }

   public void SetSpeed(float mod)
    {
        speed = speed + mod;
    }

    public void ResetBall()
    {
        speed = 4.0f;
        paused = false;
    }
}
