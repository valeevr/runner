﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimerManager : MonoBehaviour
{

    public float timer = 30.0f;
    public Text timerText;
    public GameObject player;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (player.GetComponent<PlayerMovement>().isDead)
            return;

        timer -= Time.fixedDeltaTime;
        timerText.text = timer.ToString("F1");
        if(timer<=0)
        {
            player.GetComponent<PlayerMovement>().PlayerDeath();
            player.GetComponent<PlayerMovement>().ReportEnd(player.GetComponent<PlayerMovement>().gameManager.level,"time");
        }
    }
    public void Reset()
    {
        timer = 30.0f;
        timerText.text = timer.ToString("F1");
    }
}
