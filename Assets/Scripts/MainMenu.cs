﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

public class MainMenu : MonoBehaviour {
    public GameObject player;
    public SoundManager soundManager;
    public SwipeManager swipeManager;
    public GameObject cubeGo;
    public GameObject buttonPlay;
    public GameObject Tutorial;
    public GameObject menu;
    public GameObject roads;


    public GameObject blockLeft;
    public GameObject blockRight;
    bool tutorialDone;
    int showTutorial=0;
    // Use this for initialization
    void Start () {

        player.GetComponent<Animator>().SetBool("menu", true);
        cubeGo.SetActive(false);
        Tutorial.SetActive(false);
#if UNITY_ANDROID
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
        SignIn();
        PlayerPrefs.DeleteKey("tutorial");
        tutorialDone = PlayerPrefs.HasKey("tutorial");
        Application.targetFrameRate = 30;
        QualitySettings.vSyncCount = 0;


      
#endif

    }

    // Update is called once per frame
    void Update()
    {
      

        if (showTutorial==1)
        {
            if (SwipeManager.IsSwipingLeft())
            {
                StartCoroutine(SwipeObj(0));
                showTutorial = 2;
                Tutorial.SetActive(true);

            }
        }

        if (showTutorial==2)
        { 
            if (SwipeManager.IsSwipingRight())
            {
                StartCoroutine(SwipeObj(1));
                showTutorial = 3;
               
            }
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            PlayerPrefs.DeleteKey("tutorial");
        }
    }

    public void StartGame()
    {
        soundManager.ClickSound();
        if (tutorialDone==false)
        {
            showTutorial = 1;
            //blockLeft.GetComponent<MeshRenderer>().enabled = false;
            //roads.SetActive(false);
            player.SetActive(false);
            cubeGo.SetActive(true);
            Tutorial.SetActive(true);
            menu.SetActive(false);
            //Instantiate(cubeGo, player.transform.position, Quaternion.identity);
        }
        else
        SceneManager.LoadScene("scene");
    }

    void SignIn()
    {
        Social.localUser.Authenticate(success => { });
    }

    public void ToMenu()
    {
        SceneManager.LoadScene("menu");
    }
    public void QuitGame()
    {
        soundManager.ClickSound();
        Application.Quit();
    }

    public void ShowLeaderboard()
    {

        soundManager.ClickSound();
        Social.ShowLeaderboardUI();

    }

    IEnumerator SwipeObj(int dir)
    {
        soundManager.SwipeSound();
        if (dir == 0)
        {
                //spawner.activeGO[0].transform.Translate(Vector3.left * 3.5f);
                //spawner.activeGO[0].GetComponent<Rigidbody>().MovePosition(Vector3.left * 2);
            cubeGo.GetComponent<Rigidbody>().AddForce(Vector3.left * 30, ForceMode.Impulse);
            //blockRight.GetComponent<MeshRenderer>().enabled = false;
            yield return new WaitForSeconds(0.5f);
            Tutorial.GetComponentInChildren<Text>().text = "Now swipe Right\n this will move cube to the Right side";
            //blockLeft.GetComponent<MeshRenderer>().enabled = true; ;
        }
        if (dir == 1)
        {
                //spawner.activeGO[0].GetComponent<Rigidbody>().MovePosition(Vector3.right * 2);
                //spawner.activeGO[0].transform.Translate(Vector3.right * 3.5f);
            cubeGo.GetComponent<Rigidbody>().AddForce(Vector3.right * 30, ForceMode.Impulse);
            yield return new WaitForSeconds(0.5f);
            Tutorial.GetComponentInChildren<Text>().text = "And last:\n White cubes are good, don't swipe them. \n You are ready now.";
            cubeGo.SetActive(false);
            buttonPlay.SetActive(true);
            //blockRight.SetActive(true);
            //blockRight.GetComponent<MeshRenderer>().enabled = true;
            tutorialDone = true;
        }

     
    }

   
 }

