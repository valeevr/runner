﻿using UnityEngine;
using System.Collections;

public class MouseSwipe : MonoBehaviour
{

    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;
    bool swipeStarted = false;
    bool swipeDone = false;
    public Camera cam;
   
    RaycastHit hit;
    GameObject swipedObject;

    public void Swipe()
    {

        if (SystemInfo.deviceType == DeviceType.Handheld)
        {
            if (Input.touches.Length > 0)
            {
                Touch t = Input.GetTouch(0);
                if (t.phase == TouchPhase.Began)
                {
                    Ray ray = cam.ScreenPointToRay(Input.GetTouch(0).position);
                    if (Physics.Raycast(ray, out hit))
                    {
                        if (hit.collider.gameObject.tag == "Enemy")
                        {
                            swipedObject = hit.collider.gameObject;
                        }
                    }

                    swipeStarted = true;
                    firstPressPos = new Vector2(t.position.x, t.position.y);
                }
                if (t.phase == TouchPhase.Ended)
                {
                    secondPressPos = new Vector2(t.position.x, t.position.y);
                    currentSwipe = new Vector3(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
                    currentSwipe.Normalize();
                }
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                swipeStarted = true;
                firstPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.gameObject.tag == "Enemy")
                    {
                        swipedObject = hit.collider.gameObject;
                    }
                }
            } 
            if (Input.GetMouseButtonUp(0))
            {
                secondPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
                currentSwipe.Normalize();
            }
       }
       
        if(swipeStarted==true)
        { 
        if (currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
            {
                Debug.Log("up swipe");
             
                swipeDone = true;
            }
            //swipe down
            if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
            {
                Debug.Log("down swipe");
                swipeDone = true;
            }
            //swipe left
            if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
            {
                Debug.Log("left swipe");
                swipedObject.transform.Translate(Vector3.left);
                swipeDone = true;
            }
            //swipe right
            if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
            {
                Debug.Log("right swipe");
                swipedObject.transform.Translate(Vector3.right);
                swipeDone = true;
            }
        }

        if (swipeDone==true)
        {
            swipeDone = false;
            swipeStarted = false;
          //  swipedObject = null;
        }
    }

    private void Update()
    {

        Swipe();
    }
}
