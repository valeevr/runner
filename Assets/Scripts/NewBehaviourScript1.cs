﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class NewBehaviourScript1 : MonoBehaviour
{

    float timer = 60.0f;
    public Text timerText;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.fixedDeltaTime;
        timerText.text = timer.ToString("F2");
    }
}
