﻿using UnityEngine;
using System.Collections;

public class DestroyCUbe : MonoBehaviour
{
    public GameObject spawner;
    public GameManager gameManager;
    // Use this for initialization
    void Start()
    {
        gameManager=GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        spawner = gameManager.spawner;
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter(Collision collision)
    {
        var tagCol = collision.gameObject.tag;
        if (tagCol == "Destroy")
        {
            //Debug.Log(collision.gameObject.tag);
            StartCoroutine(DestroyDelay());
            spawner.GetComponent<Spawner>().activeGO.Remove(gameObject);
            gameManager.player.GetComponent<PlayerMovement>().hitEnemy = false;


        }
    }

    IEnumerator DestroyDelay()
    {
      
        yield return new WaitForSeconds(0.25f);
        Destroy(gameObject);
    }
}
