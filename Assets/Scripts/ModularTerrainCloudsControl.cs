﻿using UnityEngine;
using System.Collections;

public class ModularTerrainCloudsControl : MonoBehaviour {

	//Range for min/max values of variable
	[Range(-10f, 10f)]
	public float cloudsMoveSpeed_x, cloudsMoveSpeed_z;

	// Clouds Movement
	void Update () {
        if(transform.position.x<25)
        { 
		gameObject.transform.Translate (cloudsMoveSpeed_x * Time.deltaTime, 0f, cloudsMoveSpeed_z * Time.deltaTime);
        }
        else
        {
            transform.position = new Vector3(-20, transform.position.y, transform.position.z);
        }
    }
}
