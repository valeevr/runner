﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {
    public float score = 0f;
    public Text scoreText;
    private int difficultyLevel = 1;
    private int maxdifficultyLevel = 10;
    private int scoretoBeat = 10;
    private bool isDead = false;
    public DeathMenu dm;
    public GameObject player;
    public Text Level;
    public LevelUp levelup; 
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (isDead == true)
            return;

        if (score>=scoretoBeat)
        {
            LevelUp();
        }
        score += Time.deltaTime;
        scoreText.text = score.ToString("F0")+" points";
	}

    void LevelUp()
    {
        scoretoBeat = scoretoBeat * 2;
        difficultyLevel++;
        //Level.text = "Level: "+difficultyLevel.ToString();
        //levelup.gameObject.SetActive(true);
    }

    public void onDeath()
    {
        isDead = true;
        if(PlayerPrefs.GetFloat("Highscore")<score)
        PlayerPrefs.SetFloat("Highscore",score);

        dm.GetComponent<DeathMenu>().ShowMenu(score);
    }

    public void ResetScore()
    {
        score = 0;
        dm.GetComponent<DeathMenu>().HideMenu();
        isDead = false;
    }
}
