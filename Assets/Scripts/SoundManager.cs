﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

public class SoundManager : MonoBehaviour
{
    [SerializeField]
    private AudioClip clickSound;

    [SerializeField]
    private AudioClip enemySound;

    [SerializeField]
    private AudioClip positiveEffect;

    [SerializeField]
    private AudioClip swipeEffect;

    [SerializeField]
    private AudioClip catched;

    AudioSource audioData;
    bool hit_enemy;
    public bool hit_speedup;
    public bool hit_shrink;
    public bool hit_slow;

    // Use this for initialization
    void Start()
    {
        audioData = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ClickSound()
    {
        audioData.clip = clickSound;
        audioData.PlayOneShot(clickSound);
    }

    public void SwipeSound()
    {
        audioData.clip = swipeEffect;
        audioData.PlayOneShot(swipeEffect);
    }

    public void CatchSound()
    {
        audioData.clip = catched;
        audioData.PlayOneShot(catched);
    }

    public void HitEnemy()
    {
        if (hit_enemy == false)
        {
            audioData.clip = enemySound;
            audioData.PlayOneShot(enemySound);
            hit_enemy = true;
        }

    }

    public void SpeedUp()
    {
        if (hit_speedup == false)
        {
            audioData.clip = positiveEffect;
            audioData.PlayOneShot(positiveEffect);
            hit_speedup = true;
        }
    }

    public void Shrink()
    {
        if (hit_shrink == false)
        {
            audioData.clip = positiveEffect;
            audioData.PlayOneShot(positiveEffect);
            hit_shrink = true;
        }
    }

    public void SlowDown()
    {
        if (hit_slow == false)
        {
            audioData.clip = enemySound;
            audioData.PlayOneShot(enemySound);
            hit_slow = true;
        }
    }


    public void ResetHits()
    {
        hit_enemy = false;
        hit_speedup = false;
        hit_shrink = false;
    }


}
