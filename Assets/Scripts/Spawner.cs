﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public Transform playerTransform;
    public List<GameObject> obstacles;
    public GameObject BlocksPool;
    public GameObject TutorialCube;
    public GameObject slowCube;
    public GameObject deathCube;
    public GameObject speedCube;
    public GameObject shrinkCube;

    Collider[] hitColliders;
    float ySpread;
    float lastYPos;
    public GameObject go;
    public float xDistance = 20;
    public float minSpread = 20;
    public float maxSpread = 40;
    private float animationDuration = 7.0f;
    private float startTime;
    float nextSpawn = 0f;
    public List<GameObject> activeGO;
    public GameManager gameManager;

    // Use this for initialization
    void Start () {
        activeGO = new List<GameObject>();
        startTime = 0;
        lastYPos = 0;
    }
	
	// Update is called once per frame
	void Update () {
        startTime+= Time.deltaTime * 5;

        if (startTime > animationDuration)
        {
            if (playerTransform.position.z - lastYPos >= ySpread)
            {
       
                float lanePos = Random.Range(0, 3);
                lanePos = (lanePos - 1) * 1.5f;
                if(gameManager.level==1)
                    go= Instantiate(TutorialCube, new Vector3(0, -0.9f, playerTransform.position.z + xDistance), Quaternion.identity) as GameObject;
                else
                go = Instantiate(obstacles[Random.Range(0, obstacles.Count)], new Vector3(0, -0.9f, playerTransform.position.z + xDistance), Quaternion.identity) as GameObject;

                go.transform.parent = BlocksPool.transform;
               /* if (go.tag == "Speed")
                {
                    var pos = Random.Range(0, 1);
                    if(pos==1)
                        go.transform.position = new Vector3(-2.5f, -0.9f, playerTransform.position.z + xDistance);
                    else
                        go.transform.position = new Vector3(2.5f, -0.9f, playerTransform.position.z + xDistance);
                }*/
                //go.transform.localScale=new Vector3(0.5f, 0.5f, 0.5f);
                lastYPos = playerTransform.position.z;
                ySpread = Random.Range(minSpread, maxSpread);
                nextSpawn = 0f;
                activeGO.Add(go);
            }
        }
    }

    public void IncreaseDistance(int dist)
    {
        xDistance = xDistance + dist;
    }

    public void ResetSpawn()
    {
        activeGO = new List<GameObject>();
        obstacles = new List<GameObject>();
        obstacles.Add(TutorialCube);
    }

}
