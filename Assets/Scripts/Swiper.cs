﻿using UnityEngine;
using System.Collections;

public class Swiper : MonoBehaviour
{
    public Camera cam;
    SwipeManager swipeManager;
    PlayerMovement player;
    GameObject BlocksPool;
    GameObject swipedObject;
    Spawner spawner;
    RaycastHit hit;
    int i = 0;
    bool swipeStarted;
    float speed;
    Ray ray;

    public GameManager gameManager;

    private void Start()
    {
        player = gameManager.player.GetComponent<PlayerMovement>();
        swipeManager = gameManager.swipeManager.GetComponent<SwipeManager>();
        spawner = gameManager.spawner.GetComponent<Spawner>();

        BlocksPool = gameManager.blocksPool;


    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (SwipeManager.IsSwipingLeft())
        {
            StartCoroutine(SwipeObj(0));
            StartCoroutine(player.Aim());

        }
        if (SwipeManager.IsSwipingRight())
        {
            StartCoroutine(SwipeObj(1));
            StartCoroutine(player.Aim());
        }
    }


    IEnumerator SwipeObj(int dir)
    {
        if (dir == 0)
        {
            if (spawner.activeGO.Count>0)
            {

                //spawner.activeGO[0].transform.Translate(Vector3.left * 3.5f);
                //spawner.activeGO[0].GetComponent<Rigidbody>().MovePosition(Vector3.left * 2);
                spawner.activeGO[0].GetComponent<Rigidbody>().AddForce(Vector3.left * 10, ForceMode.Impulse);
            }
        }
        if (dir == 1)
        {
            if (spawner.activeGO.Count > 0)
            {

                //spawner.activeGO[0].GetComponent<Rigidbody>().MovePosition(Vector3.right * 2);
                //spawner.activeGO[0].transform.Translate(Vector3.right * 3.5f);
                spawner.activeGO[0].GetComponent<Rigidbody>().AddForce(Vector3.right * 10, ForceMode.Impulse);

            }  
        }
        gameManager.soundManager.SwipeSound();
        yield return new WaitForSeconds(0.5f);
    }
}
