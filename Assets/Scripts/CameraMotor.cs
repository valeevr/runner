﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMotor : MonoBehaviour {

    public Transform lookAt;
    private Vector3 startOffset;
    private Vector3 moveVector;

    private float transition = 0.0f;
    private float animationDuration = 2.0f;
    private Vector3 animationOffset=new Vector3(0,5,5);

	// Use this for initialization
	void Start () {
        startOffset = transform.position - lookAt.transform.position;
      
	}
	
	// Update is called once per frame
	void Update () {
        moveVector = lookAt.transform.position + startOffset;
        moveVector.x = 0;
        moveVector.y = Mathf.Clamp(moveVector.y, 3, 5);
        transform.position = moveVector;

	}
}
