﻿using UnityEngine;
using System.Collections;

public class BG : MonoBehaviour
{
    public GameObject player;
    private Vector3 startOffset;
    private Vector3 moveVector;

    // Use this for initialization
    void Start()
    {
        startOffset = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        moveVector = player.transform.position + startOffset;
       // moveVector.x = 0;
        moveVector.y = -5;
        transform.position = moveVector;

    }
}
