﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class DeathMenu : MonoBehaviour {

    public Text scoreFinal;
    public Image bgImage;
    public GameManager gameManager;
    // Use this for initialization
    void Start () {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
		
	}

   

    public void ShowMenu(float score)
    {
        //scoreFinal.text = score.ToString("F0");
        gameObject.SetActive(true);
       
    }

    public void HideMenu()
    {
        gameObject.SetActive(false);
    }

    public void PlayAgain()
    {
        gameManager.soundManager.ClickSound();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ToMenu()
    {
        SceneManager.LoadScene("menu");
    }
    public void QuitGame()
    {
        gameManager.soundManager.ClickSound();
        Application.Quit();
    }

    public void ShowLeaderboard()
    {

        gameManager.soundManager.ClickSound();
        Social.ShowLeaderboardUI();

    }
}
