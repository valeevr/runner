﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour {

    public GameObject[] tilePrefabs;
    public Transform playerTransform;
    private float spawnZ=-5;
    private float tileLength = 11.0f;
    private int amountofTiles = 5;
    public float safeZone = 15;
    private int lastPrefabIndex = 0;

    private List<GameObject> activeTiles;

    // Use this for initialization
    void Start () {
        activeTiles = new List<GameObject>();

        //playerTransform=t
        for (int i = 0; i < amountofTiles; i++)
        {
            if (i < 2)
                SpawnTile(0);
            else
                SpawnTile();
        }
    }
	
	// Update is called once per frame
	void Update () {
        if(playerTransform.position.z-safeZone>(spawnZ-amountofTiles*tileLength))
            {
            SpawnTile();
            SpawnTile();
            DeleteTile();

        }
		
	}

    public void SpawnTile(int prefabIndex=-1)
    {
        GameObject go;
        if(prefabIndex==-1)
        {
            go = Instantiate(tilePrefabs[RandomPrefabIndex()]) as GameObject;
        }
        else
        { 
        go = Instantiate(tilePrefabs[prefabIndex]) as GameObject;
        }
        go.transform.SetParent(transform);
        go.transform.position = Vector3.forward * spawnZ;
        go.transform.position = new Vector3(0.0f, -2, go.transform.position.z);
        spawnZ += tileLength;
        activeTiles.Add(go);

    }

    private void DeleteTile()
    {
        Destroy(activeTiles[0]);
        activeTiles.RemoveAt(0);
    }

    private int RandomPrefabIndex()
    {
        if(tilePrefabs.Length<=1)
            return 0;
        int randomIndex = lastPrefabIndex;
        while(randomIndex==lastPrefabIndex)
        {
            randomIndex = Random.Range(0, tilePrefabs.Length);
        }
        lastPrefabIndex = randomIndex;
        return lastPrefabIndex;
    }

    public void SpawnNew()
    {
        GameObject go;
        go = Instantiate(tilePrefabs[0]) as GameObject;
        go.name = "spawnedreset";
        go.transform.SetParent(transform);
        go.transform.position = playerTransform.position;
        go.transform.position = new Vector3(go.transform.position.x, -2, go.transform.position.z);
        activeTiles.Add(go);
    }
}
